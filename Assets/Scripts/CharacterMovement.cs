using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class CharacterMovement : MonoBehaviour
{

    public enum GameType {Sideview, Topview};
    public GameType gameType = GameType.Sideview;

    public enum GameStates {Idle, Move, Jump, Dash, Skill};
    public GameStates currentState = GameStates.Idle;

    private PlayerInput refPlayerInput;
    private Collider refCollider;
    private Rigidbody rb;

    private InputAction moveAction;
    private InputAction jumpAction;


    public float speed = 8f;
    public float jumpPower = 10f;
    public float airGravityMod = 2f;
    public float dashDuration = 0.5f;
    public float dashSpeed = 16f;

    private Vector3 dashDirection;
    private float dashTimer;

    private Vector2 moveInput;
    private bool jumpInput;
    private bool dashInput;
    private bool meleeInput;

    public LayerMask groundmask;
    private bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        GameObject spawn = GameObject.FindGameObjectWithTag("Spawn");
        transform.position = spawn.transform.position;
        Destroy(spawn);

        GameObject.Find("Camera Target Group").GetComponent<CinemachineTargetGroup>().AddMember(transform, 1, 1);

        refPlayerInput = GetComponent<PlayerInput>();
        refCollider = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();


        //moveAction = refPlayerInput.actions["Movement"];
        //jumpAction = refPlayerInput.actions["Jump"];
    }

	public void OnMove(InputAction.CallbackContext context)
	{
        moveInput = context.ReadValue<Vector2>();

    }

    public void OnJump(InputAction.CallbackContext context)
    {
        jumpInput = context.action.triggered;
    }

    public void OnDash(InputAction.CallbackContext context)
    {
        dashInput = context.action.triggered;
    }

    public void OnMelee(InputAction.CallbackContext context)
    {
        meleeInput = context.action.triggered;
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(transform.position, Vector3.down, refCollider.bounds.extents.y * 1.1f, groundmask))
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }

        if (currentState == GameStates.Idle)
        {
            Jump();
            Move();
            Dash();
            Melee();

            if (moveInput.magnitude > 0.1f)
            {
                currentState = GameStates.Move;
            }

            if(grounded == false)
			{
                currentState = GameStates.Jump;
            }
        }
        else if (currentState == GameStates.Move)
        {
            Jump();
            Move();
            Dash();
            Melee();

            if (moveInput.magnitude <= 0.1f)
            {
                currentState = GameStates.Idle;
            }

            if (grounded == false)
            {
                currentState = GameStates.Jump;
            }
        }
        else if (currentState == GameStates.Jump)
        {
            Move();
            Dash();

            rb.velocity += Physics.gravity * (airGravityMod - 1) * Time.deltaTime;

            if (grounded == true && rb.velocity.y < 0)
            {
                currentState = GameStates.Idle;
            }
        }
        else if (currentState == GameStates.Dash)
        {
            rb.velocity = dashDirection * dashSpeed;

            dashTimer -= Time.deltaTime;

            if(dashTimer <= 0)
			{
                currentState = GameStates.Idle;
            }
        }
    }

	void Move()
	{
        if (gameType == GameType.Sideview)
        {
            rb.velocity = new Vector3(moveInput.x * speed, rb.velocity.y, 0.0f);
        }

        if (gameType == GameType.Topview)
        {
            rb.velocity = new Vector3(moveInput.x * speed, rb.velocity.y, moveInput.y * speed);
        }
    }

	void Jump()
	{
        if (jumpInput && grounded == true)
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpPower, 0.0f);

            currentState = GameStates.Jump;
        }
    }

    void Dash()
	{
        if(dashInput && moveInput.magnitude >= 0.1f)
		{

            if (gameType == GameType.Sideview)
            {
               dashDirection = new Vector3(moveInput.x, moveInput.y, 0.0f);
            }

            if (gameType == GameType.Topview)
            {
                dashDirection = new Vector3(moveInput.x, 0.0f, moveInput.y);
            }

            dashTimer = dashDuration;

            currentState = GameStates.Dash;
		}
	}

    void Melee()
	{
        if(meleeInput)
		{

		}
	}
}
